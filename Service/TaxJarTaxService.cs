﻿using calculatetax.Models;
using Microsoft.AspNetCore.Html;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace calculatetax.Service
{
    public class TaxJarTaxService : ICalculateTaxService
    {
        IConfiguration _config;

        public TaxJarTaxService(IConfiguration config)
        {
            _config = config;
        }

        public async Task<string> GetTaxWithLocation(TaxItem request)
        {
            // build tax request with mock data for line items and nexus address
            var items = new List<LineItems>
            {
               new LineItems
               {
                   id = request.id,
                   quantity = request.quantity,
                   product_tax_code = request.product_tax_code,
                   unit_price = request.unit_price,
                   discount = request.discount
               }
            };

            var nexus = new List<NexusAddresses>
            {
                new NexusAddresses
                {
                    id = "Main Location",
                    city = request.to_city,
                    country = "US",
                    zip = request.to_zip,
                    state = request.to_state,
                    street = request.to_street

                }
            };

            
            request.line_items = items.ToArray();
            request.nexus_addresses = nexus.ToArray();

            string result;
            using (HttpClient httpClient = new HttpClient())
            {
                var token = _config.GetValue<string>("TaxJar:ApiKey");
                var uri = _config.GetValue<string>("TaxJar:Uri");
                httpClient.DefaultRequestHeaders.Authorization =
                new AuthenticationHeaderValue("Bearer", token);
                var req = JsonSerializer.Serialize(request);
                var content = new StringContent(JsonSerializer.Serialize(request), Encoding.UTF8, "application/json");
                var response = await httpClient.PostAsync(uri, content);
                result = await response.Content.ReadAsStringAsync();
            }
           
            return result;
        }
    }
}
