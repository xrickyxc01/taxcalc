﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace calculatetax.Models
{
 
    public class TaxItem
    {
        public string from_country { get; set; }
        public string from_zip { get; set; }
        public string from_state { get; set; }
        public string from_city { get; set; }
        public string from_street { get; set; }
        public string to_country { get; set; }
        public string to_zip { get; set; }
        public string to_state { get; set; }
        public string to_city { get; set; }
        public string to_street { get; set; }
        public int amount { get; set; }
        public float shipping { get; set; }
        [System.Text.Json.Serialization.JsonIgnore]
        public string id { get; set; }
        [System.Text.Json.Serialization.JsonIgnore]
        public int quantity { get; set; }
        [System.Text.Json.Serialization.JsonIgnore]
        public string product_tax_code { get; set; }
        [System.Text.Json.Serialization.JsonIgnore]
        public int unit_price { get; set; }
        [System.Text.Json.Serialization.JsonIgnore]
        public int discount { get; set; }
        public NexusAddresses[] nexus_addresses { get; set; }
        public LineItems[] line_items { get; set; }
        [System.Text.Json.Serialization.JsonIgnore]
        public string ItemValue { get; set; }
        [System.Text.Json.Serialization.JsonIgnore]
        public string TaxCalculator { get; set; }
    }

    public class NexusAddresses
    {
        public string id { get; set; }
        public string country { get; set; }
        public string zip { get; set; }
        public string state { get; set; }
        public string city { get; set; }
        public string street { get; set; }
    }

    public class LineItems
    {
        public string id { get; set; }
        public int quantity { get; set; }
        public string product_tax_code { get; set; }
        public int unit_price { get; set; }
        public int discount { get; set; }
    }

}
