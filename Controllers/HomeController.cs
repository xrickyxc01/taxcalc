﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using calculatetax.Models;
using calculatetax.Service;
using Microsoft.Extensions.Configuration;
using System.Text.Json;

namespace calculatetax.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly ICalculatorFactory _calcFactory;

        public HomeController(ICalculatorFactory calcFactory)
        {
            _calcFactory = calcFactory;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        public async Task<IActionResult> TaxPage(TaxItem req)
        {             
            var calculator = _calcFactory.GetCalculator(req);
            var response = await calculator.GetTaxWithLocation(req);
            var model = JsonSerializer.Deserialize<TaxCalculatedViewModel>(response);
            return View("~/Views/Home/Index.cshtml", model);
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
